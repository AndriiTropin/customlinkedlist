package com.pendragonworks.customlinkedlist.list;

import com.pendragonworks.customlinkedlist.exception.IncompatibleTypesException;
import com.pendragonworks.customlinkedlist.exception.NullValueException;

/**
 * Created by Andrii Tropin on 9/7/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */

public class CustomLinkedList<E> {
    Entry<E> lastElement;
    Entry<E> firstElement;

    private void checkType(E o) {
            if (!(o instanceof Integer || o instanceof Double || o instanceof Float || o instanceof String))
            throw new IncompatibleTypesException("IncompatibleTypes");
    }

    private void checkIndex(int index) {
        if (!(index >= 0 && index < size()))
            throw new IndexOutOfBoundsException("IndexOutOfBoundsException");
    }

    private void checkNull(E o) {
        if (o == null)
            throw new NullValueException("NullValue");
    }

    public Boolean add(E o) {
        checkNull(o);
        checkType(o);
        if (lastElement == null) {
            firstElement = lastElement = new Entry(o);
        } else {
            lastElement.next = new Entry(lastElement, o);
            lastElement = lastElement.next;
        }
        return true;
    }

    public Boolean addFirst(E o) {
        add(0, o);
        return true;
    }

    public Boolean addLast(E o) {
        add(o);
        return true;
    }


    public Boolean add(int i, E o) {
        checkNull(o);
        checkType(o);
        checkIndex(i);
        Entry checkedSize = firstElement;
        Entry newObject = new Entry(o);
        int j = 0;
        while (checkedSize != null) {
            if (i == 0 || j == i - 1) {
                newObject.prev = checkedSize;
                newObject.next = checkedSize.next;
                checkedSize.next.prev = newObject;
                checkedSize.next = newObject;
                return true;
            }
            checkedSize = checkedSize.next;
            j++;
        }
        return null;
    }

    public int size() {
        if (lastElement == null) {
            return 0;
        }
        int i = 1;
        Entry checkedSize = lastElement;
        while (checkedSize != checkedSize.prev) {
            checkedSize = checkedSize.prev;
            i++;
        }
        return i;
    }


    public Boolean remove(E o) {
        checkNull(o);
        Entry checkedSize = firstElement;
        while (checkedSize != null) {
            if (checkedSize.element.equals(o)) {
                checkedSize.next.prev = checkedSize.prev;
                checkedSize.prev.next = checkedSize.next;
                return true;
            }
            checkedSize = checkedSize.next;
        }
        return null;
    }

    public Boolean remove(int i) {
        checkIndex(i);
        Entry<E> checkedSize = firstElement;
        int j = 0;
        while (checkedSize != null) {
            if (i == j) {
                checkedSize.next.prev = checkedSize.prev;
                checkedSize.prev.next = checkedSize.next;
                return true;
            }
            checkedSize = checkedSize.next;
            j++;
        }
        return null;
    }

    public Boolean remove() {
        if (firstElement == null) return true;
        firstElement = firstElement.next;
        firstElement.prev = firstElement;
        return true;
    }

    public Object getFirst() {
        if (firstElement == null) return null;
        return firstElement.element;
    }

    public Object getLast() {
        if (firstElement == null) return null;
        return lastElement.element;
    }

    public Object get(int i) {
        checkIndex(i);
        Entry checkedSize = firstElement;
        int j = 0;
        while (checkedSize != null) {
            if (i == j) return checkedSize.element;
            checkedSize = checkedSize.next;
            j++;
        }
        return null;
    }

    public Boolean contain(E o) {
        checkNull(o);
        if (indexOf(o) != -1) {
            return true;
        } else {
            return false;
        }
    }

    public int indexOf(E o) {
        int i = 0;
        Entry checkedSize = firstElement;
        do {
            if (checkedSize.next == null) return -1;
            if (checkedSize.element.equals(o)) return i;
            i++;
            checkedSize = checkedSize.next;
        } while (!checkedSize.element.equals(o));
        return i;
    }

    public Boolean clear() {
        this.firstElement = null;
        this.lastElement = null;
        return true;
    }

    public void getAll() {
        System.out.println("print all object");
        Entry checkedSize = firstElement;
        while (checkedSize != null) {
            System.out.println(checkedSize.element);
            checkedSize = checkedSize.next;
        }
    }

    public Boolean removeLast() {
        lastElement.element = null;
        lastElement.next = null;
        lastElement = lastElement.prev;
        return true;
    }

    public static class Entry<E> {
        Entry<E> prev;
        E element;
        Entry<E> next;

        Entry(E element) {
            this.prev = this;
            this.element = element;
            this.next = null;
        }

        Entry(Entry prev, E element) {
            this.prev = prev;
            this.element = element;
            this.next = null;
        }

    }
}

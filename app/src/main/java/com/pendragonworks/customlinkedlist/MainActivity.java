package com.pendragonworks.customlinkedlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pendragonworks.customlinkedlist.list.CustomLinkedList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomLinkedList myList = new CustomLinkedList();

        myList.add(1);
        myList.add(2.5);
        myList.add("3");
        myList.add(1,2.7);
        myList.addFirst(0);
        myList.addLast("4");
        myList.size();
        myList.remove("3");
        myList.remove();
        myList.size();
        myList.getFirst();
        myList.getLast();
        myList.get(1);
        myList.contain("4");
        myList.indexOf("4");
        myList.getAll();
        myList.clear();
        myList.size();
    }
}

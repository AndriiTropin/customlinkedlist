package com.pendragonworks.customlinkedlist.exception;

/**
 * Created by Andrii Tropin on 9/12/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */
public class NullValueException extends RuntimeException {
    public NullValueException() {
    }

    public NullValueException(String var1) {
        super(var1);
    }
}
package com.pendragonworks.customlinkedlist.exception;

/**
 * Created by Andrii Tropin on 9/12/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */

public class IncompatibleTypesException extends RuntimeException {
    public IncompatibleTypesException() {
    }

    public IncompatibleTypesException(String var1) {
        super(var1);
    }
}
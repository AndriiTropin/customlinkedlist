package com.pendragonworks.customlinkedlist;

import com.pendragonworks.customlinkedlist.container.Unknown;
import com.pendragonworks.customlinkedlist.exception.IncompatibleTypesException;
import com.pendragonworks.customlinkedlist.exception.NullValueException;
import com.pendragonworks.customlinkedlist.list.CustomLinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Andrii Tropin on 9/7/2017.
 * https://pendragonworks.com
 * tropin.a@pendragonworks.com
 */
public class CustomLinkedListTest {
    private CustomLinkedList myList;

    @Before
    public void setUp() throws Exception {
        myList = new CustomLinkedList();
    }

    @Test
    public void add_number() throws Exception {
        Assert.assertTrue(myList.add(1));
        Assert.assertTrue(myList.add(2.5));
    }
    @Test
    public void add_string() throws Exception {
        Assert.assertTrue(myList.add("3"));
    }

    @Test(expected = IncompatibleTypesException.class)
    public void add_unknown() throws Exception {
myList.add(new Unknown());
    }

    @Test
    public void addFirst() throws Exception {
        myList.add(1);
        Assert.assertEquals((Object) 1, myList.getFirst());
    }

    @Test
    public void addLast() throws Exception {
        myList.add(1);
        myList.add(2);
        myList.addLast(3);
        Assert.assertEquals(3, myList.getLast());
    }

    @Test
    public void add_index() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        myList.add(1, 0.7);
        Assert.assertEquals((Object) 0.7, myList.get(1));

    }

    @Test
    public void size() throws Exception {
        myList.add(1);
        Assert.assertEquals(1, myList.size());
        myList.add(1);
        myList.add(1);
        Assert.assertEquals(3, myList.size());
    }

    @Test
    public void remove() throws Exception {
        myList.add(1);
        myList.add(2);
        Assert.assertEquals(true, myList.remove());
        Assert.assertEquals(2, myList.getFirst());
        Assert.assertEquals(1, myList.size());
    }

    @Test
    public void remove_last() throws Exception {
        myList.add(1);
        myList.add(2);
        Assert.assertEquals(true, myList.removeLast());
        Assert.assertEquals(1, myList.getFirst());
        Assert.assertEquals(1, myList.size());
    }

    @Test
    public void remove_index() throws Exception {
        myList.add(1);
        myList.add(2);
        myList.add(3);
        Assert.assertEquals(true, myList.remove(1));
        Assert.assertEquals(2, myList.size());
    }

    @Test
    public void remove_object() throws Exception {
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.remove("1");
        Assert.assertEquals((Object) "2", myList.get(1));
    }

    @Test
    public void getFirst() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        Assert.assertEquals((Object) 0, myList.getFirst());
    }

    @Test
    public void getLast() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        Assert.assertEquals((Object) 2, myList.getLast());
    }

    @Test
    public void get() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        Assert.assertEquals((Object) 2, myList.get(2));
    }

    @Test
    public void contain() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        Assert.assertTrue(myList.contain((Object) 0));
    }

    @Test
    public void indexOf() throws Exception {
        myList.add(0);
        myList.add(1);
        myList.add(2);
        Assert.assertEquals(2,myList.indexOf((Object) 2));
    }

    @Test
    public void clear() throws Exception {
        myList.add(1);
        myList.clear();
        Assert.assertEquals(0, myList.size());
    }

    @Test
    public void getAll() throws Exception {
        myList.add(1);
        myList.add(2.5);
        myList.add("3");
        myList.getAll();
    }

    @Test(expected = NullValueException.class)
    public void addNull() throws Exception {
        myList.add(null);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOfBounds() throws Exception {
        myList.get(1);
    }

}